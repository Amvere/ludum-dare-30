require "element"

GRID_X_START = 4
GRID_Y_START = 220

GRID_X_COUNT = 4
GRID_Y_COUNT = 6
GRID_X_MAX_INDEX = GRID_X_COUNT - 1
GRID_Y_MAX_INDEX = GRID_Y_COUNT - 1

GRID_SPACE_BETWEEN = 8 --the gap between the 4 main grids

--required number of adjacent square with the same color for them to be considered a match
MININIMUM_MATCH_COUNT = 4

currentScore = 0
currentLevel = 0
targetScore = 1000
gameState = "intro"

countSinceLastSpawn = 0

--arrays for the levels to be different
targetScores = { 5000, 7000, 11000, 15000 } 
spawnCounts = { 4, 3, 2, 1 }

function love.load()
   --love.graphics.setNewFont(12)
   --love.graphics.setColor(0,0,0)
   --love.graphics.setBackgroundColor(255,255,255)
   loadElementImages()
   love.window.setTitle("Connectris")

   bBackground = love.graphics.newImage("images/blueBackground.png")
   yBackground = love.graphics.newImage("images/yellowBackground.png")
   rBackground = love.graphics.newImage("images/redBackground.png")
   gBackground = love.graphics.newImage("images/greenBackground.png")
   love.window.setMode( bBackground:getWidth() * 2, 808, {} )
   startupNoise = love.audio.newSource("sounds/startupNoise.wav", "static") 
   
   clearElements()
end

function clearElements()

   
   elements = {}

   --blue section (top left)
   for i=1,GRID_X_COUNT,1 do
       elements[i] = {}
       for j=1,GRID_Y_COUNT,1 do
           elements[i][j] = Element.create("empty", GRID_X_START + ((i-1)*squareWidth) , GRID_Y_START + ((j-1)*squareHeight))
       end
   end

    --yellow section (top right)
    for i=1,GRID_X_COUNT,1 do
       elements[i+GRID_X_COUNT] = {}
       for j=0,GRID_Y_COUNT,1 do
           elements[i+GRID_X_COUNT][j] = Element.create("empty", GRID_X_START + ((i-1+GRID_X_COUNT)*squareWidth) + GRID_SPACE_BETWEEN, GRID_Y_START + ((j-1)*squareHeight))
       end
    end

    --red section (bottom left)
    for i=1,GRID_X_COUNT,1 do
       for j=1,GRID_Y_COUNT,1 do
           elements[i][j+GRID_Y_COUNT] = Element.create("empty", GRID_X_START + ((i-1)*squareWidth) , GRID_Y_START + ((j-1+GRID_Y_COUNT)*squareHeight) + GRID_SPACE_BETWEEN)
       end
    end

    --green section (bottom right)
    for i=1,GRID_X_COUNT,1 do
       for j=1,GRID_Y_COUNT,1 do
           elements[i+GRID_X_COUNT][j+GRID_Y_COUNT] = Element.create("empty", GRID_X_START + ((i-1+GRID_X_COUNT)*squareWidth) + GRID_SPACE_BETWEEN, GRID_Y_START + (((j-1+GRID_Y_COUNT)*squareHeight) + GRID_SPACE_BETWEEN))
       end
    end

end

lastTime = love.timer.getTime()
function love.update(dt)
    if(currentScore >= 0) then
        test = 1
    end
    if(targetScore >=0) then
        test = 2
    end
    if(currentScore >= targetScore) then
        --don't have time to debug array indexing, so hardcoding the match
        if(targetScore == 15000) then
            gameState = "gameWin"
        else
            gameState = "levelWin"
        end
    elseif(gameState == "running") then

        processSubgridMatches()

        local time = love.timer.getTime()
        if(time - lastTime > .55)then
            lastTime = time

            gravity()
            countSinceLastSpawn = countSinceLastSpawn + 1
            if(countSinceLastSpawn >= countBetweenNewSpawns) then
                countSinceLastSpawn = 0
                addNewElement(1)
            end

        end

        for i in ipairs(elements) do
            for j in ipairs(elements[i]) do
                elements[i][j]:update()
            end
        end
    end
end

function love.draw()
   local currTextY = 5
   local lineDelta = 15
    if(gameState == "intro") then
        love.graphics.print("Connectris",bBackground:getWidth() - 30,244)
    elseif(gameState == "running") then
       love.graphics.print("Click on a square to swap with the next world!", 10, currTextY )
       currTextY = currTextY + lineDelta
       love.graphics.print("Swaps go to the next world, counter-clockwise", 10, currTextY)
       currTextY = currTextY + lineDelta
       love.graphics.print( "Connect " .. MININIMUM_MATCH_COUNT .. " or more of the same color to make a match", 10, currTextY)
       currTextY = currTextY + lineDelta
       love.graphics.print( "You can make matches anywhere, but you get triple points\n if you match with the background color too!", 10, currTextY)
       currTextY = currTextY + lineDelta*2
       love.graphics.print( "Matching extra squares beyond " .. MININIMUM_MATCH_COUNT .. "also gives bonus points", 10, currTextY)
       currTextY = currTextY + lineDelta
       love.graphics.print( "    Double for " .. (MININIMUM_MATCH_COUNT + 1) .. ", Triple for " .. (MININIMUM_MATCH_COUNT + 2) .. ", ect...", 10, currTextY)
       currTextY = currTextY + lineDelta
       love.graphics.print( "You lose if the stacks grow higher than the top of any world", 10, currTextY)
       currTextY = currTextY + lineDelta*2


       love.graphics.print("Controls:", 10, currTextY)
       currTextY = currTextY + lineDelta
       love.graphics.print("Press 'r' or 'escape' to reset", 10+30, currTextY)
       currTextY = currTextY + lineDelta
       love.graphics.print("Press 'spacebar' to pause", 10+30, currTextY)
       currTextY = currTextY + lineDelta
       
       love.graphics.print( "Level " .. currentLevel .. "     " .. "Current score: " .. currentScore .. "     Score to win: " .. targetScore, 10, currTextY+lineDelta)

       --love.graphics.print( "i = " .. seeval1 .. "j = " .. seeval2, currTextY, 20)
       local bgWidth = rBackground:getWidth()
       local bgHeight = rBackground:getHeight()
       love.graphics.draw(bBackground, GRID_X_START - (GRID_SPACE_BETWEEN/2), GRID_Y_START - (GRID_SPACE_BETWEEN)/2)
       love.graphics.draw(yBackground, GRID_X_START - (GRID_SPACE_BETWEEN/2) + bgWidth, GRID_Y_START - (GRID_SPACE_BETWEEN)/2)
       love.graphics.draw(rBackground, GRID_X_START - (GRID_SPACE_BETWEEN/2), GRID_Y_START + bgHeight - (GRID_SPACE_BETWEEN)/2)
       love.graphics.draw(gBackground, GRID_X_START - (GRID_SPACE_BETWEEN/2) + bgWidth, GRID_Y_START + bgHeight - (GRID_SPACE_BETWEEN)/2)

        for i in ipairs(elements) do
            for j in ipairs(elements[i]) do
                elements[i][j]:draw()
            end
        end
    elseif(gameState == "gameOver") then
       love.graphics.print( "Game Over :'(", bBackground:getWidth() - 30,244)
    elseif(gameState == "levelWin") then
       love.graphics.print( "Woohoo!! You beat the level!", bBackground:getWidth() - 100,244)
       love.graphics.print( "The next one will be harder  =)", bBackground:getWidth() - 100,244+lineDelta+lineDelta)
    elseif(gameState == "gameWin") then
       love.graphics.print( "You beat the whole game!?", 110,244+lineDelta)
       love.graphics.print( "Dude...what a boss.", 150,244+lineDelta+lineDelta*4)
    elseif(gameState == "paused") then
       love.graphics.print( "The game is now paused", 110,244+lineDelta)
       love.graphics.print( "What a nice feature!", 150,244+lineDelta+lineDelta*4)
    else
    end

end

function love.mousepressed(x, y, button)
    if button == 'l' then
        if(gameState == "intro" or gameState == "levelWin" or gameState == "gameOver") then
            if(gameState ~= "gameOver") then
                currentLevel = currentLevel + 1
            end
            gameState = "running"
            startupNoise:play()
            currentScore = 0
            clearElements()
            countBetweenNewSpawns = spawnCounts[currentLevel]
            targetScore = targetScores[currentLevel]
        elseif(gameState == "running") then
            for i in ipairs(elements) do
                for j in ipairs(elements[i]) do
                    clicked = elements[i][j]:checkClick(x,y)
                    if(clicked) then
                        rotate(i,j)
                    end
                end
            end
        else
        end
    end
end

function love.keypressed(key)
   if key == ' ' then
       if gameState == "running" then
           gameState = "paused"
       elseif gameState == "paused" then
           gameState = "running"
       end
   elseif key == 'r' or key == 'R' or key == 'escape' then
        startupNoise:play()
        currentScore = 0
        clearElements()
    end

end

seeval1 = 0
seeval2 = 0
math.randomseed( os.time() )

function rotate(iVal,jVal)
    local element = elements[iVal][jVal]
    if(iVal <= GRID_X_COUNT and jVal <= GRID_Y_COUNT) then --top left
        newI = iVal
        newJ = jVal + GRID_Y_COUNT
    elseif(iVal > GRID_X_COUNT and jVal <= GRID_Y_COUNT) then --top right
        newI = iVal - GRID_X_COUNT
        newJ = jVal
    elseif(iVal <= GRID_X_COUNT) then --bottom left
        newI = iVal + GRID_X_COUNT
        newJ = jVal
    else --bottom right
        newI = iVal
        newJ = jVal - GRID_Y_COUNT
    end

    if(elements[iVal][jVal].state == "empty"
   or elements[iVal][jVal].state == "removing"
   or elements[newI][newJ].state == "removing") then
        --do nothing...could have notted, but this is easier to read imo
    else
        elements[iVal][jVal].x, elements[newI][newJ].x = elements[newI][newJ].x, elements[iVal][jVal].x
        elements[iVal][jVal].y, elements[newI][newJ].y = elements[newI][newJ].y, elements[iVal][jVal].y
        elements[iVal][jVal], elements[newI][newJ] = elements[newI][newJ], elements[iVal][jVal]
        elements[iVal][jVal]:handleSwap(false)
        elements[newI][newJ]:handleSwap(true)
    end

end

--if I want to use count ~= 1, I am going to have to make sure to not
--spawn in the same place which would cause a lame game over
function addNewElement(count)
    for i=1,count,1 do
        local addedI = math.random(GRID_X_COUNT)

        local iOffset = GRID_X_COUNT * math.random(0,1)
        local jOffset = GRID_Y_COUNT * math.random(0,1)

        if(elements[addedI+iOffset][1+jOffset].state == "empty") then
            elements[addedI+iOffset][1+jOffset]:spawn()
        else
            gameState = "gameOver"
        end
    end
end

function gravity()
    gravityHelper(0,0)
    gravityHelper(0,GRID_Y_COUNT)
    gravityHelper(GRID_X_COUNT,0)
    gravityHelper(GRID_X_COUNT,GRID_Y_COUNT)
end

function gravityHelper(iOffset, jOffset)
    for i=1+iOffset,GRID_X_COUNT+iOffset,1 do
        for j=GRID_Y_COUNT+jOffset,1+jOffset,-1 do
            if(j % GRID_Y_COUNT ~= 0 and elements[i][j+1].state == "empty" and elements[i][j].state ~= "removing") then
                elements[i][j].x, elements[i][j+1].x = elements[i][j+1].x, elements[i][j].x
                elements[i][j].y, elements[i][j+1].y = elements[i][j+1].y, elements[i][j].y
                elements[i][j], elements[i][j+1] = elements[i][j+1], elements[i][j]
            end
        end
    end

end

function processSubgridMatches()
    hasSubgridMatches(1,1)
    hasSubgridMatches(1,GRID_Y_COUNT + 1)
    hasSubgridMatches(GRID_X_COUNT + 1,1)
    hasSubgridMatches(GRID_X_COUNT + 1,GRID_Y_COUNT + 1)
end

--helper for hasSubgridMatches
function resetChecked(initialI, initialJ)
    checked = {}
    for i=initialI,initialI + GRID_X_COUNT,1 do
        checked[i] = {}
        for j=initialJ,initialJ + GRID_Y_COUNT,1 do
            checked[i][j] = false
        end
    end
end

--helper function for hasMatches
function hasSubgridMatches(initialI, initialJ)
    for i=initialI,initialI + GRID_X_MAX_INDEX,1 do
        for j=initialJ,initialJ + GRID_Y_MAX_INDEX,1 do
            resetChecked(initialI, initialJ)
            matches = {}
            local count = 0
            count = recursiveCheck(i,j,elements[i][j].color)
            if(count >= MININIMUM_MATCH_COUNT ) then
                matches[i*GRID_X_COUNT + j] = { tableI = i, tableJ = j }
                for index in pairs(matches) do
                    elements[matches[index].tableI][matches[index].tableJ]:remove()
                end
                local colorMultiplier = 1
                if((initialI == 1 and initialJ == 1 and elements[i][j].color == "blue")
                or (initialI == GRID_X_COUNT + 1 and initialJ == 1 and elements[i][j].color == "yellow")
                or (initialI == 1 and initialJ == GRID_Y_COUNT + 1 and elements[i][j].color == "red")
                or (initialI == GRID_X_COUNT + 1 and initialJ == GRID_Y_COUNT + 1 and elements[i][j].color == "green")) then
                    colorMultiplier = 3
                    seeval1 = 1
                end
                extraMatchMuliplier = 1 + count - MININIMUM_MATCH_COUNT
                currentScore = currentScore + (count * 100 * colorMultiplier * extraMatchMuliplier)
            end
        end
    end

end

--checks for matches
function recursiveCheck(iVal, jVal, color)
    local matchCount = 0

    if(elements[iVal][jVal].color == color 
   and elements[iVal][jVal].state ~= "empty" 
   and elements[iVal][jVal].state ~= "removing"
   and checked[iVal][jVal] ~= true) then
        checked[iVal][jVal] = true
        matchCount = matchCount + 1
        matches[iVal*GRID_X_COUNT + jVal] = { tableI = iVal, tableJ = jVal }
        if( jVal % GRID_Y_COUNT ~= 0 ) then
            matchCount = matchCount + recursiveCheck(iVal, jVal+1, elements[iVal][jVal].color)
        end
        if( iVal % GRID_X_COUNT ~= 0 ) then
            matchCount = matchCount + recursiveCheck(iVal+1, jVal, elements[iVal][jVal].color)
        end
        if( jVal % GRID_Y_COUNT ~= 1 ) then
            matchCount = matchCount + recursiveCheck(iVal, jVal-1, elements[iVal][jVal].color)
        end
        if( iVal % GRID_X_COUNT ~= 1 ) then
            matchCount = matchCount + recursiveCheck(iVal-1, jVal, elements[iVal][jVal].color)
        end
    end
    return matchCount
end

