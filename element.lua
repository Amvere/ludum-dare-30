Element = {}
Element.__index = Element

function loadElementImages()
   rSquare = love.graphics.newImage("images/redSquare.png")
   ySquare = love.graphics.newImage("images/yellowSquare.png")
   bSquare = love.graphics.newImage("images/blueSquare.png")
   gSquare = love.graphics.newImage("images/greenSquare.png")
   removalSquare = love.graphics.newImage("images/graySquare.png")
   squareWidth = rSquare:getWidth()
   squareHeight = rSquare:getHeight()
   rCircle = love.graphics.newImage("images/redCircle.png")
   yCircle = love.graphics.newImage("images/yellowCircle.png")
   bCircle = love.graphics.newImage("images/blueCircle.png")
   gCircle = love.graphics.newImage("images/greenCircle.png")
   swapNoise = love.audio.newSource("sounds/swapNoise.wav", "static") 
   matchFoundNoise = love.audio.newSource("sounds/matchFoundNoise.wav", "static") 
   blocksRemovedNoise = love.audio.newSource("sounds/blocksRemovedNoise.wav", "static") 
end

function Element.create(color, x, y)
    local elmnt = {}
    setmetatable(elmnt,Element)
    elmnt.x = x
    elmnt.y = y
    elmnt.w = rSquare:getWidth()
    elmnt.state = "empty"
    elmnt.color = color
    elmnt.swapTime = 0
    elmnt.removeTime = -1 --this is important as the default
    return elmnt
end

function Element:update()
    local currentTime = love.timer.getTime()
    if(self.state ~= "empty" and self.state ~= "removing") then
        if (currentTime - self.swapTime < .35) then
        --if (self.swapTime ~= 0) then
            self.state = "circle"
        else
            self.state = "square"
        end
    end

    if(self.removeTime ~= -1 and currentTime - self.removeTime > .75) then
        blocksRemovedNoise:play()
        self.removeTime = -1
        self.state = "empty"
    end
end

function Element:draw()
    local circleImage
    local squareImage
    if(self.color == "blue") then
        circleImage = bCircle
        squareImage = bSquare
    elseif(self.color == "yellow") then
        circleImage = yCircle
        squareImage = ySquare
    elseif(self.color == "red") then
        circleImage = rCircle
        squareImage = rSquare
    else
        circleImage = gCircle
        squareImage = gSquare
    end
        
    if(self.state == "circle") then
        love.graphics.draw(circleImage, self.x, self.y)
    elseif(self.state == "square") then
        love.graphics.draw(squareImage, self.x, self.y)
    elseif(self.state == "removing") then
        love.graphics.draw(removalSquare, self.x, self.y)
    else
    end
end

math.randomseed( os.time() )
function Element:spawn()
    color = math.random(1,4)
    if(color == 1) then
        self.color = "blue"
    elseif(color == 2) then
        self.color = "yellow"
    elseif(color == 3) then
        self.color = "red"
    else
        self.color = "green"
    end
    self.state = "square"
end

function Element:remove()
    self.state = "removing"
    self.removeTime = love.timer.getTime()
    matchFoundNoise:play()
end

--return true if the mouse click was within this element
function Element:checkClick(x,y)
    --TODO change this to be for circle bounds...
    return(x > self.x and x < self.x + self.w and y > self.y and y < self.y + self.w)
end

function Element:handleSwap(playSound)
    self.swapTime = love.timer.getTime()
    if(playSound == true) then
        swapNoise:play()
    end
end


